package com.supyuan.component.util;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.DES;

import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JFlyFoxUtils {
    /**
     * 管理员
     */
    public static final int USER_TYPE_ADMIN = 1;
    /**
     * 普通用户
     */
    public static final int USER_TYPE_NORMAL = 2;
    /**
     * 前台用户
     */
    public static final int USER_TYPE_FRONT = 3;
    /**
     * 第三方用户
     */
    public static final int USER_TYPE_THIRD = 4;
    /**
     * API用户
     */
    public static final int USER_TYPE_API = 5;
    /**
     * 其他用户
     */
    public static final int USER_TYPE_OTHER = 9;


    /**
     * session唯一Key
     */
    public static final String USER_KEY = "USER_KEY";

    public static final int MENU_ABOUT = 90;

    // admin:4poP7wBk4ZY= test:GGAljAzT4gg=
    public static void main(String[] args) {
        String password = "admin";
        String tmp = passwordEncrypt(password);
        System.out.println(tmp);
        System.out.println(passwordDecrypt(tmp));

        password = "test";
        tmp = passwordEncrypt(password);
        System.out.println(tmp);
        System.out.println(passwordDecrypt(tmp));
    }

    public static DES getDes() {
        try {
            return SecureUtil.des(Constants.KEY_STR.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 编码
     * <p>
     * 2015年2月25日 下午2:22:08 flyfox 369191470@qq.com
     *
     * @param password
     * @return
     */
    public static String passwordEncrypt(String password) {
        return getDes().encryptBase64(password);
    }

    /**
     * 解码
     * <p>
     * 2015年2月25日 下午2:22:13 flyfox 369191470@qq.com
     *
     * @param encryptPassword
     * @return
     */
    public static String passwordDecrypt(String encryptPassword) {
        return getDes().decryptStr(encryptPassword);
    }

    /**
     * cookie编码
     * <p>
     * 2015年2月25日 下午2:22:08 flyfox 369191470@qq.com
     *
     * @param password
     * @return
     */
    public static String cookieEncrypt(String password) {
        return getDes().encryptBase64(password);
    }

    /**
     * cookie解码
     * <p>
     * 2015年2月25日 下午2:22:13 flyfox 369191470@qq.com
     *
     * @param encryptPassword
     * @return
     */
    public static String cookieDecrypt(String encryptPassword) {
        return getDes().decryptStr(encryptPassword);
    }

    /**
     * 默认密码
     * <p>
     * 2015年2月25日 下午2:23:37 flyfox 369191470@qq.com
     *
     * @return
     */
    public static String getDefaultPassword() {
        String defaultPassword = "123456";
        return passwordEncrypt(defaultPassword);
    }

    /**
     * 删除侵入脚本
     * <p>
     * 2015年6月20日 下午5:16:21 flyfox 369191470@qq.com
     *
     * @param htmlStr
     * @return
     */
    public static String delScriptTag(String htmlStr) {
        Pattern p_script = Pattern.compile("<script[^>]*?>[\\s\\S]*?<\\/script>", 2);
        Matcher m_script = p_script.matcher(htmlStr);
        htmlStr = m_script.replaceAll("");
        Pattern p_style = Pattern.compile("<style[^>]*?>[\\s\\S]*?<\\/style>", 2);
        Matcher m_style = p_style.matcher(htmlStr);
        htmlStr = m_style.replaceAll("");
        return htmlStr.trim();
    }


    /**
     * 是否是后台请求地址
     * <p>
     * 2015年2月27日 上午11:38:37 flyfox 369191470@qq.com
     *
     * @param path
     * @return
     */
    public static boolean isBack(String path) {
        // 后台不需要认证页面
        if (path.startsWith("admin/login")  //
                || path.startsWith("admin/logout")
                || path.startsWith("admin/imageCodes")) {
            return false;
        }

        return StrUtil.isNotEmpty(path) // 空是首页
                && (path.startsWith("admin") // 后台管理
                || path.startsWith("admin/")
                || (path.startsWith("system") // 系统管理
                || path.startsWith("system/"))
        );
    }
}
