/**
 * Copyright 2015-2025 FLY的狐狸(email:jflyfox@sina.com qq:369191470).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.supyuan.component.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;
import com.jfinal.kit.StrKit;
import com.supyuan.component.util.Config;

import java.io.IOException;
import java.util.regex.Pattern;

public class BasePathHandler extends Handler {
    private String basePathName;

    public BasePathHandler() {
        basePathName = "BASE_PATH";
    }

    public BasePathHandler(String contextPathName) {
        if (StrKit.isBlank(contextPathName)) {
            throw new IllegalArgumentException("contextPathName can not be blank.");
        } else {
            this.basePathName = contextPathName;
            return;
        }
    }

    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean isHandled[]) {

        //对连接上的 显式 可疑注入脚本拦截
        String value = request.getRequestURI();
        Pattern alertPattern = Pattern.compile("[Aa]lert", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
        Pattern scriptPattern = Pattern.compile("[Ss]cript", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
        boolean bol = alertPattern.matcher(value).find() || scriptPattern.matcher(value).find();
        if(bol){
            try {
                response.sendRedirect("/");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            // 注入脚本过滤


            // target 这里对部分进行拦截 系统后台相关的不做拦截 前台文章发布的不拦截 自由拓展
            /*if(!(target.lastIndexOf("article/save") >0 || target.lastIndexOf("article/publish")>0)){
                if (!(target.startsWith("/admin/") || target.startsWith("/system/") ||
                        target.startsWith("/ueditor/") || target.startsWith("/banner/"))) {
                    request = new XssRequestWrapper(request);
                }
            }*/
        }

        setBasePathPath(request);
        setContextPath(request);
        setCurrentPath(request);
        setSysUrl(request);
        next.handle(target, request, response, isHandled);
    }


    /******
     * 全路径获取
     * @param request
     */
    public void setBasePathPath(HttpServletRequest request){
        int port = request.getServerPort();
        String path = request.getContextPath();

        String basePath = request.getScheme() + "://" + request.getServerName()
                + ":" + request.getServerPort() + path + "/";
        if (port == 80) {
            basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        }
        request.setAttribute(basePathName, basePath);
        request.setAttribute("ctx", basePath);
    }


    /******
     * 根目录获取
     * @param request
     */
    public void setContextPath(HttpServletRequest request){
        request.setAttribute(Config.getStr("PATH.CONTEXT_PATH"), request.getContextPath());
    }


    /******
     * 当前获取
     * @param request
     */
    public void setCurrentPath(HttpServletRequest request){
        String currentPath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        currentPath += request.getRequestURI(); // 参数
        String currentPathParam = currentPath;
        if (request.getQueryString() != null) // 判断请求参数是否为空
            currentPathParam += "?" + request.getQueryString(); // 参数
        // 无参数
        request.setAttribute(Config.getStr("PATH.CURRENT_PATH"), currentPath);
        // 有参数
        request.setAttribute(Config.getStr("PATH.CURRENT_PATH") + "_PARAM", currentPathParam);
    }

    /******
     * 系统相关
     * @param request
     */
    public void setSysUrl(HttpServletRequest request){
        //自由拓展
        request.setAttribute("sysVersion", "v3.3");
    }

}
