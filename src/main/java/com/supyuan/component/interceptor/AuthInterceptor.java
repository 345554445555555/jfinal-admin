package com.supyuan.component.interceptor;

import cn.hutool.core.util.StrUtil;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.supyuan.component.base.BaseProjectController;
import com.supyuan.component.util.Attr;
import com.supyuan.modules.system.auth.SysAuth;
import com.supyuan.modules.system.user.SysUser;

/**
 * 权限功能拦截的
 *
 * @Author 袁旭云【rain.yuan@transn.com】
 * Created by rain on 2019/12/19.
 * @Date 2019/12/19 15:39
 */
public class AuthInterceptor implements Interceptor {

    @Override
    public void intercept(Invocation inv) {
        String tmpPath = inv.getActionKey();
        if (tmpPath.startsWith("/")) {
            tmpPath = tmpPath.substring(1, tmpPath.length());
        }
        if (tmpPath.endsWith("/")) {
            tmpPath = tmpPath.substring(0, tmpPath.length() - 1);
        }

       /*
       // TODO 这里展示控制第三方用户和前端用户不能登录后台
                int usertype = user.getInt("usertype");
                if (usertype == 4 // 第三方用户
                        || usertype == 3) { // 前端用户
                    controller.redirect("/trans/auth");
                    return;
                }

                // TODO 判断url是否有权限
                if (!urlAuth(controller, tmpPath)) {
                    controller.redirect("/trans/auth");
                    return;
                }
       // TODO 判断功能操作是否有权限
        if (!uriAuth(user, tmpPath, controller)) {

            String script = "history.back();";
            controller.setAttr("script", script);
            controller.render("/pages/template/message.html");

            return;
        }*/

        Controller controller = inv.getController();
        // TODO 判断功能操作是否有权限
        if (!uriAuth(tmpPath, controller)) {
            String script = "history.back();";
            controller.setAttr("script", script);
            controller.render("/pages/template/message.html");
        } else {
            inv.invoke();
        }
    }

    /**
     * 权限验证
     *
     * @param tmpPath
     * @return
     */
    protected boolean uriAuth(String tmpPath, Controller controller) {

        // 每次访问获取session，没有可以从cookie取~
        SysUser user = null;
        if (controller instanceof BaseProjectController) {
            user = (SysUser) ((BaseProjectController) controller).getSessionUser();
        } else {
            user = controller.getSessionAttr(Attr.SESSION_NAME);
        }

        SysAuth userAuth = SysAuth.dao.findFirstByWhere("WHERE webapp_id = ? AND uri = ?", 99, tmpPath);
        boolean has = false;
        if (null != user && null != userAuth && !"T".equalsIgnoreCase(userAuth.getStr("is_halt"))) {
            Integer userId = user.getUserID();
            Integer webappId = 99;
            String sql = "SELECT t.* FROM sys_auth_user t WHERE t.webapp_id = ?  AND t.user_id = ?";
            Record rd = Db.findFirst(sql, webappId, userId);
            if (null != rd) {
                String cacheString = rd.getStr("cache_string");
                if (userId == 1 && "*".equals(cacheString)) {
                    has = true;
                } else if (StrUtil.isNotBlank(cacheString)) {
                    cacheString = ",".concat(cacheString).concat(",");
                    if (cacheString.indexOf(",".concat(Long.toString(userAuth.getInt("id")).concat(","))) != -1) {
                        has = true;
                    }
                }
            }
        } else {
            has = true;
        }
        if (!has) {
            controller.setAttr("msg", "[" + userAuth.getStr("descript") + "]" + "权限不足!");
            return false;
        }
        return true;
    }

}
