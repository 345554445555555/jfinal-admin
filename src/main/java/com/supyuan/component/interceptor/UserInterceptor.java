package com.supyuan.component.interceptor;

import cn.hutool.core.util.StrUtil;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.supyuan.component.base.BaseProjectController;
import com.supyuan.component.util.Attr;
import com.supyuan.component.util.Config;
import com.supyuan.component.util.IpUtils;
import com.supyuan.component.util.JFlyFoxUtils;
import com.supyuan.modules.system.menu.SysMenu;
import com.supyuan.modules.system.user.SysUser;

import java.util.Enumeration;
import java.util.List;

/**
 * 用户认证拦截器
 *
 * @author flyfox 2014-2-11
 */
public class UserInterceptor implements Interceptor {

    private static final Log log = Log.getLog(UserInterceptor.class);

    public void intercept(Invocation ai) {
        Controller controller = ai.getController();
        StringBuffer paramsSb = new StringBuffer("");
        Enumeration enu = controller.getParaNames();
        if (null != enu) {
            while (enu.hasMoreElements()) {
                String paraName = (String) enu.nextElement();
                String paraVal = controller.getPara(paraName);
                paramsSb.append(paraName + ":" + paraVal + ", ");
            }
        }

        String ip = IpUtils.getClientIP(controller.getRequest());
        log.info("访问者IP#IP：" + ip + "\t请求路径:" + ai.getActionKey() + "\t参数:{" + paramsSb.toString() + "}");

        String tmpPath = ai.getActionKey();
        if (tmpPath.startsWith("/")) {
            tmpPath = tmpPath.substring(1, tmpPath.length());
        }
        if (tmpPath.endsWith("/")) {
            tmpPath = tmpPath.substring(0, tmpPath.length() - 1);
        }
        if (!(controller instanceof BaseProjectController)) {
            controller.render(Config.getStr("PAGES.PLAT"));
        } else {
            // 每次访问获取session，没有可以从cookie取~
            SysUser user = null;
            if (controller instanceof BaseProjectController) {
                user = (SysUser) ((BaseProjectController) controller).getSessionUser();
            } else {
                user = controller.getSessionAttr(Attr.SESSION_NAME);
            }
            if (JFlyFoxUtils.isBack(tmpPath)) {
                //访问后台，没有登录的直接去登录页面
                if (user == null) {
                    controller.redirect("admin/logout");
                } else if ((user != null && user.getUserid() <= 0)) {
                    controller.redirect("admin/logout");
                } else {
                    //登录了放行，后面的拦截在 AuthInterceptor 中处理
                    ai.invoke();
                }
            } else {
                //登录退出走这里
                ai.invoke();
            }
        }
    }

    /**
     * 判断Url是否有权限
     * <p>
     * 2016年12月18日 上午12:12:51
     *
     * @param controller
     * @param tmpPath
     * @return
     */
    protected boolean urlAuth(Controller controller, String tmpPath) {
        List<SysMenu> list = controller.getSessionAttr("nomenu");
        // nomenuList 应该是size等于0，而不是空
        if (list == null) {
            return false;
        }

        for (SysMenu sysMenu : list) {
            String url = sysMenu.getStr("url");
            if (StrUtil.isEmpty(url)) {
                continue;
            }
            if (tmpPath.startsWith(url)) {
                return false;
            }
        }

        return true;
    }

}
