package com.supyuan.component.beelt;

import com.supyuan.component.util.extend.HtmlUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.beetl.core.Format;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 *
 * XSSDefenseFormat 使用HTML实体转义<code>string<code>中的字符。
 * * <p>
 * 防止XSS攻击全称跨站脚本攻击
 * </p>
 * 页面使用 ${item.url,xss}
 * @Author 袁旭云【rain.yuan@transn.com】
 * @Date 2017/11/6 14:50
 */
public class XSSDefenseFormat  implements Format {
    @Override
    public Object format(Object o, String s) {
        if (null == o) {
            return null;
        } else {
            //return StringEscapeUtils.escapeHtml((String) o);
            String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式
            Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
            Matcher m_html = p_html.matcher((String) o);
            String txt = o.toString();
            if(!m_html.find()) {
                txt = StringEscapeUtils.unescapeHtml4(txt);
            }
            return HtmlUtils.changeSpecialCode(txt);
        }
    }
}
