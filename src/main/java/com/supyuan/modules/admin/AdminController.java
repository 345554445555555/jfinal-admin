package com.supyuan.modules.admin;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.supyuan.component.base.BaseProjectController;
import com.supyuan.component.util.DateUtils;
import com.supyuan.component.util.ImageCode;
import com.supyuan.component.util.JFlyFoxUtils;
import com.jfinal.component.annotation.ControllerBind;
import com.supyuan.modules.system.log.SysLog;
import com.supyuan.modules.system.menu.SysMenu;
import com.supyuan.modules.system.user.SysUser;
import com.supyuan.component.util.Config;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * adminController
 */
@ControllerBind(controllerKey = "admin")
public class AdminController extends BaseProjectController {


    public static final String loginPage = "/pages/admin/newLogin.html";
    //public static final String loginPage = "/pages/plat/index.html";
    public static final String homePage = "/admin/home";
    private static final String home_path = "/pages/admin/home/";

    public void index() {
        if (getSessionUser() != null) {
            // 如果session存在，不再验证
            redirect(homePage);
        } else {
            render(loginPage);
        }
    }

    public void home() {
        SysUser user = (SysUser) getSessionUser();
        if (StrUtil.isBlankIfStr(user)) {
            render(loginPage);
            return;
        }
        //默认
        render(home_path + "home.html");
        //layui main main2 都可以
        //render(home_path + "main2.html");
    }

    /**
     * 登录
     *
     * @author flyfox 2013-11-11
     */
    public void login() {
        // 获取验证码
        String imageCode = getSessionAttr(ImageCode.class.getName());
        String checkCode = this.getPara("imageCode");

        if (StrUtil.isEmpty(imageCode) || !imageCode.equalsIgnoreCase(checkCode)) {
            setAttr("msg", "验证码错误！");
            render(loginPage);
            return;
        }

        // 初始化数据字典Map
        String username = getPara("username");
        String password = getPara("password");

        if (StrUtil.isEmpty(username)) {
            setAttr("msg", "用户名不能为空");
            render(loginPage);
            return;
        } else if (StrUtil.isEmpty(password)) {
            setAttr("msg", "密码不能为空");
            render(loginPage);
            return;
        }
        //String encryptPassword = JFlyFoxUtils.passwordEncrypt(password); //
        // 前台md5加密
        String encryptPassword = password;

        SysUser user = SysUser.dao.findFirstByWhere(" where username = ? " //
                        + " and usertype in ( " + JFlyFoxUtils.USER_TYPE_ADMIN + "," + JFlyFoxUtils.USER_TYPE_NORMAL + ")",
                username);
        if (user == null || user.getInt("userid") <= 0) {
            setAttr("msg", "认证失败，请您重新输入。");
            render(loginPage);
            return;
        }

        String md5Password = "";
        try {
            String userPassword = user.get("password");
            String decryptPassword = JFlyFoxUtils.passwordDecrypt(userPassword);
            md5Password = SecureUtil.md5().digestHex(decryptPassword);
        } catch (Exception e) {
            log.error("认证异常", e);
            setAttr("msg", "认证异常，请您重新输入。");
            render(loginPage);
            return;
        }

        if (!md5Password.equals(encryptPassword)) {
            setAttr("msg", "密码错误，请您重新输入。");
            render(loginPage);
            return;
        }

        if (!(user.getInt("usertype") == 1 || user.getInt("usertype") == 2)) {
            setAttr("msg", "您没有登录权限，请您重新输入。");
            render(loginPage);
            return;
        }

        setSessionUser(user);

        // 第一个页面跳转
        String tmpMainPage = setFirstPage();

        if (tmpMainPage == null) {
            setAttr("msg", "没有权限，请联系管理员。");
            render(loginPage);
            return;
        }

        // 添加日志
        user.put("update_id", user.getUserid());
        user.put("update_time", getNow());
        saveLog(user, SysLog.SYSTEM_LOGIN);
        //redirect(tmpMainPage);
        index();
    }


    /**
     * 获取第一个跳转页面
     * <p>
     * 2015年10月30日 上午8:51:14 flyfox 330627517@qq.com
     *
     * @return
     */
    protected String setFirstPage() {
        Map<Integer, List<SysMenu>> map = getSessionAttr("menu");
        if (map == null || map.size() <= 0) {
            return null;
        }

        String tmpMainPage = "";
        List<String> menuList = new ArrayList<String>();

        List<SysMenu> list = map.get(0);
        for (SysMenu menu : list) {
            if (StrUtil.isNotEmpty(menu.getStr("url"))) {
                menuList.add("/" + menu.getStr("url"));
            }
            List<SysMenu> list2 = map.get(menu.getInt("id"));
            if (list2 == null || list2.size() < 0) {
                continue;
            }

            for (SysMenu menu2 : list2) {
                if (StrUtil.isNotEmpty(menu2.getStr("url"))) {
                    menuList.add("/" + menu2.getStr("url"));
                }
            }

        }

        if (menuList.size() <= 0) {
            return null;
        }

        if (menuList.contains(homePage)) {
            tmpMainPage = homePage;
        } else {
            tmpMainPage = menuList.get(0);
        }

        if (!tmpMainPage.startsWith("/")) {
            tmpMainPage = "/" + tmpMainPage;
        }
        return tmpMainPage;
    }

    /**
     * 登出
     *
     * @author flyfox 2013-11-13
     */
    public void logout() {
        SysUser user = (SysUser) getSessionUser();
        if (user != null) {
            // 添加日志
            user.put("update_id", user.getUserid());
            user.put("update_time", getNow());
            saveLog(user, SysLog.SYSTEM_LOGOUT);
            // 删除session
            removeSessionUser();
        }

        setAttr("msg", "您已退出");
        render(loginPage);
    }

    public void imageCodes() {
        try {
            new ImageCode().doGet(getRequest(), getResponse());
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        renderNull();
    }

    public void trans() {
        String redirectPath = getPara();
        if (StrUtil.isEmpty(redirectPath)) {
            redirectPath = Config.getStr("PAGES.TRANS");
        } else if (redirectPath.equals("auth")) {
            redirectPath = "/pages/error/trans_no_auth.html";
        }
        render(redirectPath);
    }
}
